Les IA faibles changent de case tous les 5 tours si elles sont bloquées (cf tauxDeChangement)

Tournois :
La fonction placeur renvoie la place que devrait avoir le Xième joueur dans une seed d'une taille donnée
J'ai choisi de fonctionner comme cela parceque beaucoup de méthodes de séparations ne permettent que la séparation du premier et 2nd joueurs.
Ma version permet séparer les joueurs de telle sorte que le tours avant le dernier ( et ceux d'avant récursivement) offre une répartition optimale :
Ainsi quand la seed sera de taille 4 : 1vs3 et 2vs4
	de taille 8 : 1vs5 3vs7 2vs6 4vs8
	l'éclatement est ainsi maximal

Le son apporté en jeu n'est pas celui que je souhaitait à la base mais il semble y avoir un problème de codec que je ne maîtrise pas (deux fichiers pourtant en .wav)

Mon IA peut ne pas être efficace dans certains cas mais reste la plus équilibrée (que ce soit avec une augmentation de l'appartion des pokemons, une baisse de l'efficacité de l'utilisation de bonbons ou une modification de la taille du GameWorld (densité de dresseurs))

Les explications plus en détails sont dans la doc et/ou en commentaire.