package joueurs;

import java.util.*;

import game.Bonbon;
import game.GameWorld;
import game.GameWorld.CelluleData;
import game.Pokemon;
import game.TestPokemonJava;

public class AshKetchum extends Joueur {
    private int niveau;
    private String typeDominant1; // Ex Eau
    private String typeDominant2; // Ex Feu
    private String typeFaiblesse; // Type sur lequel mes Pokemons n'ont aucune dominance de préférence sut typeDominant1 //Ex Eau
    private boolean phaseXP;

    /**
     * Constructeur de AshKetchum
     * 
     * @param ID
     *            id du joueur
     */
    public AshKetchum(int ID) {
	mesPokemons = new ArrayList<>();
	mesBonbons = new TreeMap<>();
	phaseXP = true;
	vivant = true;
	id = ID;
    }

    @Override
    public void ajoutePokemon(Pokemon p) {
	mesPokemons.add(p);
	trie();
    }

    /**
     * Créer un comparateur pour trier la liste par rapport aux niveaux.
     */
    public void trie() {
	Comparator<Pokemon> nivSup = (Pokemon a, Pokemon b) -> {
	    return (b.getNiveau() - a.getNiveau());
	};
	Collections.sort(mesPokemons, nivSup);
	// System.out.println("Triage par niveau : "); for (int i = 0; i < mesPokemons.size(); i++) { System.out.println(mesPokemons.get(i).getNiveau()); }
    }

    @Override
    public Pokemon choisirPokemon() {
	if (mesPokemons.isEmpty())
	    return null;
	utiliseBonbon();
	return mesPokemons.get(0);
    }

    @Override
    public Joueur copy(int id) {
	// Rend une copie du AshKetchum
	return new AshKetchum(id);
    }

    @Override
    public int getNiveau() {
	return this.niveau;
    }

    @Override
    public String getStrongestPokemonType() {
	if (!vivant)
	    return "Mort";
	else if (mesPokemons.size() > 0)
	{
	    String typeDominant = "";
	    int lvlMax = 0;
	    for (int compteur = 0; compteur < mesPokemons.size(); compteur++)
	    {
		if (mesPokemons.get(compteur).getNiveau() >= lvlMax)
		    typeDominant = mesPokemons.get(compteur).getType();
	    }
	    return typeDominant;
	}
	return "";
    }

    @Override
    public JoueurAction nextAction() {
	boolean pkmnPres = false;
	String aDef = "";
	String aType = "";
	String pkmnNom = "";
	String pkmnType = "";
	int pkmnNiveau = -1;
	boolean plusDe2 = false; // False si doit capturer son deuxième pokemon
	trie();
	if (mesPokemons.get(0).getNiveau() >= 3) // Détermine a partir de quel niveau l'on considère que l'XP est suffisante pour capturer des pokémons et créer des bonbons
	    phaseXP = false;
	creerBonbon();

	// Recherche des types
	typeDominant1 = mesPokemons.get(0).getType();
	if (mesPokemons.size() >= 2)
	{
	    plusDe2 = true;
	    typeDominant2 = mesPokemons.get(1).getType();
	    if (typeDominant1 == "Feu" && typeDominant2 == "Eau" || typeDominant2 == "Eau" && typeDominant2 == "Feu")
	    {
		typeFaiblesse = "Eau";
	    } else if (typeDominant1 == "Herbe" && typeDominant2 == "Eau"
		    || typeDominant1 == "Eau" && typeDominant2 == "Herbe")
	    {
		typeFaiblesse = "Herbe";
	    } else if (typeDominant1 == "Herbe" && typeDominant2 == "Feu"
		    || typeDominant1 == "Feu" && typeDominant2 == "Herbe")
	    {
		typeFaiblesse = "Feu";
	    }
	}

	// Recueil des cellules du GameWorld
	GameWorld.CelluleData currentCell = TestPokemonJava.getGameWorldData().get(getPositionX()).get(getPositionY());

	// Recherche de pokemons dans la cellule
	for (int i = 0; i < currentCell.cellCreatures.size(); i++)
	{
	    // Cherches les pokemons combatables/capturable
	    if (currentCell.cellCreatures.get(i).nom.charAt(0) == 'P')
	    {
		pkmnPres = true;
		pkmnType = currentCell.cellCreatures.get(i).type;
		pkmnNiveau = currentCell.cellCreatures.get(i).niveau;
		for (int y = 1; y < currentCell.cellCreatures.get(i).nom.length(); y++)
		{
		    pkmnNom += currentCell.cellCreatures.get(i).nom.charAt(y);
		}
		break;
	    }
	}
	if (phaseXP && pkmnPres || pkmnType == typeDominant1 || (plusDe2 && pkmnPres && pkmnType != typeFaiblesse))
	// Fais XP le pkmn tant qu'il est faible pour capturer ensuite
	// Fight si pas de possibilité d'obenir un 2nd pokemon d'un autre type que celui du dominant1
	// Si Type faiblesse : intéressant de transfo en bonbon
	{
	    // Cas "fight";
	    aType = "fight";
	    aDef = pkmnNom;
	} else if (pkmnPres && pkmnNiveau - 1 <= mesPokemons.get(0).getNiveau())
	{
	    // Cas "capture";
	    aType = "capture";
	    aDef = pkmnNom;
	} else
	{
	    // Cas "move"
	    aType = "move";
	    List<List<GameWorld.CelluleData>> cellData = TestPokemonJava.getGameWorldData();
	    aDef = pokemonProche(cellData); // Cherches si il y a des pokemons proches
	    while (aDef == "") // Va en bas à gauche de préférence si il peut
	    {
		if (getPositionX() > 0 && cellData.get(getPositionX() - 1).get(getPositionY()).active) // premiere vérification nécessaire pour ne pas avoir de null
		{
		    aDef = "Left";
		    break;
		}
		if (getPositionY() > 0 && cellData.get(getPositionX()).get(getPositionY() - 1).active)
		{
		    aDef = "Down";
		    break;
		}
		if (getPositionX() < cellData.size() - 1 && cellData.get(getPositionX() + 1).get(getPositionY()).active)
		{
		    aDef = "Right";
		    break;
		}
		if (getPositionY() < cellData.get(getPositionX()).size() - 1
			&& cellData.get(getPositionX()).get(getPositionY() + 1).active)
		{
		    aDef = "Up";
		    break;
		}
	    }
	}
	this.niveau = mesPokemons.get(0).getNiveau();
	return new JoueurAction(aType, aDef);
    }

    /**
     * @param cellData
     *            toutes les cellules du GameWorld
     * @return une position où il y a un pokemon capturable/combatable ou ""
     */
    private String pokemonProche(List<List<CelluleData>> cellData) {
	if (getPositionX() > 0 && cellData.get(getPositionX() - 1).get(getPositionY()).active) // Premiere vérification nécessaire pour ne pas avoir de null
	{
	    for (int i = 0; i < cellData.get(getPositionX() - 1).get(getPositionY()).cellCreatures.size(); i++) // Parcours les créatures
	    {
		if (cellData.get(getPositionX() - 1).get(getPositionY()).cellCreatures.get(i).nom.charAt(0) == 'P')
		    if (!phaseXP || cellData.get(getPositionX() - 1).get(getPositionY()).cellCreatures.get(i).niveau
			    + 1 <= mesPokemons.get(0).getNiveau()) // Ne cherche pas de pkmn trop fort après l'XP
			return "Left";
	    }
	}
	if (getPositionX() < cellData.size() - 1 && cellData.get(getPositionX() + 1).get(getPositionY()).active)
	{
	    for (int i = 0; i < cellData.get(getPositionX() + 1).get(getPositionY()).cellCreatures.size(); i++)
	    {
		if (cellData.get(getPositionX() + 1).get(getPositionY()).cellCreatures.get(i).nom.charAt(0) == 'P')
		    if (!phaseXP || cellData.get(getPositionX() + 1).get(getPositionY()).cellCreatures.get(i).niveau
			    + 1 <= mesPokemons.get(0).getNiveau())
			return "Right";
	    }
	}
	if (getPositionY() < cellData.get(getPositionX()).size() - 1
		&& cellData.get(getPositionX()).get(getPositionY() + 1).active)
	{
	    for (int i = 0; i < cellData.get(getPositionX()).get(getPositionY() + 1).cellCreatures.size(); i++)
	    {
		if (cellData.get(getPositionX()).get(getPositionY() + 1).cellCreatures.get(i).nom.charAt(0) == 'P')
		    if (!phaseXP || cellData.get(getPositionX()).get(getPositionY() + 1).cellCreatures.get(i).niveau
			    + 1 <= mesPokemons.get(0).getNiveau())
			return "Up";
	    }
	}
	if (getPositionY() > 0 && cellData.get(getPositionX()).get(getPositionY() - 1).active)
	{
	    for (int i = 0; i < cellData.get(getPositionX()).get(getPositionY() - 1).cellCreatures.size(); i++)
	    {
		if (cellData.get(getPositionX()).get(getPositionY() - 1).cellCreatures.get(i).nom.charAt(0) == 'P')
		    if (!phaseXP || cellData.get(getPositionX()).get(getPositionY() - 1).cellCreatures.get(i).niveau
			    + 1 <= mesPokemons.get(0).getNiveau())
			return "Down";
	    }
	}
	return "";
    }

    /**
     * Echange la place de deux pokémons dans la liste mesPokemons. Ancien Prérequis fixed : le pokemon p est AVANT le pokemon p2 dans la liste
     * 
     * @param p
     *            place dans mesPokemons du premier pokemon à échanger(base 0)
     * @param p2
     *            place du second
     */
    public void echangePkmn(int p, int p2) {
	if (!(p == p2))
	{
	    Pokemon tmp = mesPokemons.get(p);
	    Pokemon tmp2 = mesPokemons.get(p2);
	    mesPokemons.remove(p);
	    mesPokemons.add(p, tmp2);
	    mesPokemons.remove(p2);
	    mesPokemons.add(p2, tmp);
	}
    }

    @Override
    public boolean getVivant() {
	return vivant;
    }

    @Override
    public String mort() {
	vivant = false;
	Scanner sc = new Scanner(System.in);
	//System.out.println("AshKetchum " + id + " disqualified. Enter Last words :" );
	//return sc.nextLine();
	return "";
    }

    @Override
    public void utiliseBonbon() {
	while (!mesBonbons.get(mesPokemons.get(0).getType()).isEmpty())
	{
	    System.out.println(Bonbon.consumeBonbon(mesPokemons, mesBonbons.get(mesPokemons.get(0).getType())));
	}
    }

    @Override
    public void prepareBattle(String type) {
	if (mesPokemons.size() >= 2)
	{
	    if (type == typeFaiblesse && typeDominant2 == typeFaiblesse
		    && mesPokemons.get(1).getNiveau() + 1 >= mesPokemons.get(0).getNiveau())
		echangePkmn(0, 1);
	    else if (type == faiblesse(typeDominant1)
		    && mesPokemons.get(1).getNiveau() + 2 >= mesPokemons.get(0).getNiveau())
		echangePkmn(0, 1);
	    // Pas besoin d'autre else : car déjà trié si type == faiblesse(typeDominant2)
	}
    }

    public String faiblesse(String type) {
	switch (type) {
	case "Herbe":
	    return "Feu";
	case "Feu":
	    return "Eau";
	case "Eau":
	    return "Herbe";
	}
	return "";
    }

    @Override
    public List<Pokemon> montreTroisPokemon() {
	if (mesPokemons.isEmpty())
	    return null;
	int i = 0;
	List<Pokemon> retour = new ArrayList<>();
	while (i < mesPokemons.size() && i < 3)
	{
	    retour.add(mesPokemons.get(i));
	    i++;
	}
	return retour;
    }

    @Override
    public void creerBonbon() {
	if (!mesBonbons.containsKey("Feu"))
	{
	    mesBonbons.put("Feu", new ArrayList<>());
	    mesBonbons.put("Eau", new ArrayList<>());
	    mesBonbons.put("Herbe", new ArrayList<>());
	}
	if (mesPokemons.size() > 2)
	{
	    trie();
	    echangePkmn(0, mesPokemons.size() - 1); // Met le dernier en premier
	    mesBonbons.get(mesPokemons.get(0).getType()).add(Bonbon.creerBonbon(mesPokemons)); // Collecte du bonbon
	    mesBonbons.put(mesPokemons.get(0).getType(), mesBonbons.get(mesPokemons.get(0).getType())); // Stockage
	    trie();
	}
    }
}
