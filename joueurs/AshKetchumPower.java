package joueurs;

import java.util.*;

import game.Bonbon;
import game.GameWorld;
import game.GameWorld.CelluleData;
import game.Pokemon;
import game.TestPokemonJava;

public class AshKetchumPower extends Joueur {
    private int niveau;
    private String typeDominant1; // Ex Eau
    private String typeDominant2; // Ex Feu
    private String typeFaiblesse; // Type sur lequele mes Pokemons n'ont aucune dominance de préférence sut typeDominant1 //Ex Eau
    private int tours;

    /**
     * Constructeur de AshKetchumPower
     * 
     * @param ID
     *            id du joueur
     */
    public AshKetchumPower(int ID) {
	mesPokemons = new ArrayList<>();
	mesBonbons = new TreeMap<>();
	tours = 0;
	vivant = true;
	id = ID;
    }

    @Override
    public void ajoutePokemon(Pokemon p) {
	if (mesPokemons.isEmpty())
	    mesPokemons.add(p);
	else
	{
	    int tmp = mesPokemons.size();
	    for (int i = 0; i < tmp; i++)
	    {
		if (p.getNiveau() > mesPokemons.get(i).getNiveau())
		{
		    mesPokemons.add(i, p);
		    break;
		}
	    }
	    // Trie à l'ajout mais on peut utiliser un comparateur et le tri dédié ci-dessous
	}
    }

    public void trie() {
	// Créer un comparateur pour trier la liste par rapport aux niveaux.
	Comparator<Pokemon> nivSup = (Pokemon a, Pokemon b) -> {
	    return (b.getNiveau() - a.getNiveau());
	};
	Collections.sort(mesPokemons, nivSup);

	// System.out.println("Triage par niveau : "); for (int i = 0; i < mesPokemons.size(); i++) { System.out.println(mesPokemons.get(i).getNiveau()); }
    }

    @Override
    public Pokemon choisirPokemon() {
	if (mesPokemons.isEmpty())
	    return null;
	else if (mesPokemons.size() >= 2 && mesPokemons.get(0).getNiveau() > mesPokemons.get(1).getNiveau() + 2)
	    echangePkmn(0, 1); // pour faire xp le 2nd pok
	return mesPokemons.get(0);
    }

    @Override
    public Joueur copy(int id) {
	// Rend une copie du AshKetchumPower
	return new AshKetchumPower(id);
    }

    @Override
    public int getNiveau() {
	return this.niveau;
    }

    @Override
    public String getStrongestPokemonType() {
	if (!vivant)
	    return "Mort";
	else if (mesPokemons.size() > 0)
	{
	    String typeDominant = "";
	    int lvlMax = 0;
	    for (int compteur = 0; compteur < mesPokemons.size(); compteur++)
	    {
		if (mesPokemons.get(compteur).getNiveau() >= lvlMax)
		    typeDominant = mesPokemons.get(compteur).getType();
	    }
	    return typeDominant;
	}
	return "";
    }

    @Override
    public JoueurAction nextAction() {
	this.tours++;
	boolean pkmnPres = false;
	String aDef = "";
	String aType = "";
	String pkmnNom = "";
	String pkmnType = "";
	Random rand = new Random();
	boolean plusDe2 = false; // False si doit capturer son deuxième pokemon
	trie();
	creerBonbon();

	// Recherche des types
	typeDominant1 = mesPokemons.get(0).getType();
	if (mesPokemons.size() >= 2)
	{
	    //System.out.println("Ash possède nb pok : " + mesPokemons.size());
	    plusDe2 = true;
	    typeDominant2 = mesPokemons.get(1).getType();
	    if (typeDominant1 == "Feu" && typeDominant2 == "Eau" || typeDominant2 == "Eau" && typeDominant2 == "Feu")
	    {
		typeFaiblesse = "Eau";
	    } else if (typeDominant1 == "Herbe" && typeDominant2 == "Eau"
		    || typeDominant1 == "Eau" && typeDominant2 == "Herbe")
	    {
		typeFaiblesse = "Herbe";
	    } else if (typeDominant1 == "Herbe" && typeDominant2 == "Feu"
		    || typeDominant1 == "Feu" && typeDominant2 == "Herbe")
	    {
		typeFaiblesse = "Feu";
	    }
	}
	// Recueil des cellules du GameWorld
	GameWorld.CelluleData currentCell = TestPokemonJava.getGameWorldData().get(getPositionX()).get(getPositionY());

	// Recherche de pokemons dans la cellule
	for (int i = 0; i < currentCell.cellCreatures.size(); i++)
	{
	    // Cherches les pokemons combatables/capturable
	    if (currentCell.cellCreatures.get(i).nom.charAt(0) == 'P') //&& currentCell.cellCreatures.get(i).niveau < mesPokemons.get(0).getNiveau()
	    {
		pkmnPres = true;
		pkmnType = currentCell.cellCreatures.get(i).type;
		for (int y = 1; y < currentCell.cellCreatures.get(i).nom.length(); y++)
		{
		    pkmnNom += currentCell.cellCreatures.get(i).nom.charAt(y);
		}
		break;
	    }
	}
	if (pkmnPres)
	// Fais XP le pkmn pendant les premiers tours
	// Bouge si pas de possibilité d'obenir un 2nd pokemon d'un autre type que celui du dominant1
	// Tente de capturer absolument si 1 seul pokemon dans l'équipe
	// Si Type faiblesse : intéressant de transfo en bonbon
	{
	    // Cas "fight";
	    aType = "fight";
	    aDef = pkmnNom;
	/*} else if (pkmnPres)
	{
	    // Cas "capture";
	    aType = "capture";
	    aDef = pkmnNom;
	    System.out.println("Ash tente capture");*/
	} else
	{
	    // Cas "move"
	    aType = "move";
	    List<List<GameWorld.CelluleData>> cellData = TestPokemonJava.getGameWorldData();
	    aDef = pokemonProche(cellData);
	    while (aDef == "")
	    {
		if (getPositionX() > 0 && cellData.get(getPositionX() - 1).get(getPositionY()).active) // premiere vérification nécessaire pour ne pas avoir de null
		{
		    aDef = "Left";
		    break;
		}
		if (getPositionX() < cellData.size() - 1 && cellData.get(getPositionX() + 1).get(getPositionY()).active)
		{
		    aDef = "Right";
		    break;
		}
		if (getPositionY() < cellData.get(getPositionX()).size() - 1
			&& cellData.get(getPositionX()).get(getPositionY() + 1).active)
		{
		    aDef = "Up";
		    break;
		}
		if (getPositionY() > 0 && cellData.get(getPositionX()).get(getPositionY() - 1).active)
		{
		    aDef = "Down";
		    break;
		}
	    }
	}
	this.niveau = mesPokemons.get(0).getNiveau();
	return new JoueurAction(aType, aDef);
    }

    private String pokemonProche(List<List<CelluleData>> cellData) {
	if (getPositionX() > 0 && cellData.get(getPositionX() - 1).get(getPositionY()).active) // premiere vérification nécessaire pour ne pas avoir de null
	{
	    for (int i = 0; i < cellData.get(getPositionX() - 1).get(getPositionY()).cellCreatures.size(); i++)
		if (cellData.get(getPositionX() - 1).get(getPositionY()).cellCreatures.get(i).nom.charAt(0) == 'P')
		    return "Left";
	}
	if (getPositionX() < cellData.size() - 1 && cellData.get(getPositionX() + 1).get(getPositionY()).active)
	{
	    for (int i = 0; i < cellData.get(getPositionX() + 1).get(getPositionY()).cellCreatures.size(); i++)
		if (cellData.get(getPositionX() + 1).get(getPositionY()).cellCreatures.get(i).nom.charAt(0) == 'P')
		    return "Right";
	}
	if (getPositionY() < cellData.get(getPositionX()).size() - 1
		&& cellData.get(getPositionX()).get(getPositionY() + 1).active)
	{
	    for (int i = 0; i < cellData.get(getPositionX()).get(getPositionY() + 1).cellCreatures.size(); i++)
		if (cellData.get(getPositionX()).get(getPositionY() + 1).cellCreatures.get(i).nom.charAt(0) == 'P')
		    return "Up";
	}
	if (getPositionY() > 0 && cellData.get(getPositionX()).get(getPositionY() - 1).active)
	{
	    for (int i = 0; i < cellData.get(getPositionX()).get(getPositionY() - 1).cellCreatures.size(); i++)
		if (cellData.get(getPositionX()).get(getPositionY() - 1).cellCreatures.get(i).nom.charAt(0) == 'P')
		    return "Down";
	}
	return "";
    }
    /**
     * pokemonProche utilise eachcell pour simplififer la lecture de la case où l'on fait le test
     * 
     * @param cellData
     *            est la cellule autour de laquelle on regarde si il y a des pokemons
     * @return Une direction où il ya des pokemons
     */
    /*
     * private String pokemonProche(List<List<CelluleData>> cellData) { CelluleData eachCell = cellData.get(getPositionX() - 1).get(getPositionY()); if (eachCell.active) for (int i = 0; i < eachCell.cellCreatures.size(); i++) if (eachCell.cellCreatures.get(i).nom.charAt(0) == 'P') return "Left"; eachCell = cellData.get(getPositionX() + 1).get(getPositionY()); if (eachCell.active) for (int i = 0; i < eachCell.cellCreatures.size(); i++) if (eachCell.cellCreatures.get(i).nom.charAt(0) == 'P') return "Right"; eachCell = cellData.get(getPositionX()).get(getPositionY() + 1); if (eachCell.active) for (int i = 0; i < eachCell.cellCreatures.size(); i++) if (eachCell.cellCreatures.get(i).nom.charAt(0) == 'P') return "Up"; eachCell = cellData.get(getPositionX()).get(getPositionY() - 1); if (eachCell.active) for (int i = 0; i < eachCell.cellCreatures.size(); i++) if (eachCell.cellCreatures.get(i).nom.charAt(0) == 'P') return "Down"; return ""; }
     */

    /**
     * Echange la place de deux pokémons dans la liste mesPokemons. Ancien Prérequis fixed : le pokemon p est AVANT le pokemon p2 dans la liste
     * 
     * @param p
     *            place dans mesPokemons du premier pokemon à échanger(base 0)
     * @param p2
     *            place du second
     */
    public void echangePkmn(int p, int p2) {
	if (!(p == p2))
	{
	    Pokemon tmp = mesPokemons.get(p);
	    Pokemon tmp2 = mesPokemons.get(p2);
	    mesPokemons.remove(p);
	    mesPokemons.add(p, tmp2);
	    mesPokemons.remove(p2);
	    mesPokemons.add(p2, tmp);
	}
    }

    @Override
    public boolean getVivant() {
	return vivant;
    }

    @Override
    public String mort() {
	vivant = false;
	return "AshKetchumPower " + id + " disqualified.";
    }

    @Override
    public void utiliseBonbon() {
	while (!mesBonbons.get(mesPokemons.get(0).getType()).isEmpty())
	{
	    Bonbon.consumeBonbon(mesPokemons, mesBonbons.get(mesPokemons.get(0).getType()));
	}
    }

    @Override
    public void prepareBattle(String type) {
	// on suppose 1 seul pkmn
	utiliseBonbon();
    }

    public String faiblesse(String type) {
	switch (type) {
	case "Herbe":
	    return "Feu";
	case "Feu":
	    return "Eau";
	case "Eau":
	    return "Herbe";
	}
	return "";
    }

    @Override
    public List<Pokemon> montreTroisPokemon() {
	if (mesPokemons.isEmpty())
	    return null;
	int i = 0;
	List<Pokemon> retour = new ArrayList<>();
	while (i < mesPokemons.size() && i < 3)
	{
	    retour.add(mesPokemons.get(i));
	    i++;
	}
	return retour;
    }

    @Override
    public void creerBonbon() {
	if (!mesBonbons.containsKey("Feu"))
	{
	    mesBonbons.put("Feu", new ArrayList<>());
	    mesBonbons.put("Eau", new ArrayList<>());
	    mesBonbons.put("Herbe", new ArrayList<>());
	}
	if (mesPokemons.size() > 2)
	{
	    trie();
	    echangePkmn(0, mesPokemons.size() - 1); // Met le dernier en premier
	    mesBonbons.get(mesPokemons.get(0).getType()).add(Bonbon.creerBonbon(mesPokemons)); // Collecte du bonbon
	    mesBonbons.put(mesPokemons.get(0).getType(), mesBonbons.get(mesPokemons.get(0).getType())); // Stockage
	    trie();
	}
    }
}
