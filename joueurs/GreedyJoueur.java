package joueurs;

import java.util.*;

import game.Bonbon;
import game.GameWorld;
import game.Pokemon;
import game.TestPokemonJava;

public class GreedyJoueur extends Joueur {
    private int niveau;

    int samePlace;
    final int tauxDeChangement = 5;

    /**
     * Constructeur de GreedyJoueur
     * 
     * @param ID
     *            id du joueur
     */
    public GreedyJoueur(int ID) {
	mesPokemons = new ArrayList<>();
	mesBonbons = new TreeMap<>();
	vivant = true;
	id = ID;
    }

    @Override
    public void ajoutePokemon(Pokemon p) {
	mesPokemons.add(p);
	trie();
    }

    public void trie() {
	// Créer un comparateur pour trier la liste par rapport aux niveaux.
	Comparator<Pokemon> nivSup = (Pokemon a, Pokemon b) -> {
	    return (b.getNiveau() - a.getNiveau());
	};
	Collections.sort(mesPokemons, nivSup);

	// System.out.println("Triage par niveau : "); for (int i = 0; i < mesPokemons.size(); i++) { System.out.println(mesPokemons.get(i).getNiveau()); }
    }

    @Override
    public Pokemon choisirPokemon() {
	if (mesPokemons.isEmpty())
	    return null;
	else
	    return mesPokemons.get(0);

    }

    @Override
    public Joueur copy(int id) {
	// Rend une copie du GreedyJoueur
	return new GreedyJoueur(id);
    }

    @Override
    public int getNiveau() {
	return this.niveau;
    }

    @Override
    public String getStrongestPokemonType() {
	if (!vivant)
	    return "Mort";
	else if (mesPokemons.size() > 0)
	{
	    String typeDominant = "";
	    int lvlMax = 0;
	    for (int compteur = 0; compteur < mesPokemons.size(); compteur++)
	    {
		if (mesPokemons.get(compteur).getNiveau() >= lvlMax)
		    typeDominant = mesPokemons.get(compteur).getType();
	    }
	    return typeDominant;
	}
	return "";
    }

    @Override
    public JoueurAction nextAction() {
	// System.out.println("Greedy " + getID() + " possède :");
	boolean pkmnPres = false;
	String aDef = "";
	String aType = "";
	String pkmnNom = "";
	Random rand = new Random();

	trie();
	creerBonbon();

	GameWorld.CelluleData currentCell = TestPokemonJava.getGameWorldData().get(getPositionX()).get(getPositionY()); // Pour obtenir la cellule actuelle
	for (int i = 0; i < currentCell.cellCreatures.size(); i++)
	{
	    if (currentCell.cellCreatures.get(i).nom.charAt(0) == 'P')
	    {
		pkmnPres = true;
		for (int y = 1; y < currentCell.cellCreatures.get(i).nom.length(); y++)
		{
		    pkmnNom += currentCell.cellCreatures.get(i).nom.charAt(y);
		}
		break;
	    }
	}
	if (pkmnPres && samePlace < tauxDeChangement)
	{
	    samePlace++;
	    // Cas "capture"
	    aType = "capture";
	    aDef = pkmnNom;

	} else
	{
	    samePlace = 0;
	    // Cas "move"
	    aType = "move";
	    List<List<GameWorld.CelluleData>> cellData = TestPokemonJava.getGameWorldData();
	    while (aDef == "")
	    {
		switch (rand.nextInt(4)) {
		case 0:
		    if (getPositionX() > 0 && cellData.get(getPositionX() - 1).get(getPositionY()).active)
			aDef = "Left";
		    break;
		case 1:
		    if (getPositionX() < cellData.size() - 1
			    && cellData.get(getPositionX() + 1).get(getPositionY()).active)
			aDef = "Right";
		    break;
		case 2:
		    if (getPositionY() < cellData.get(getPositionX()).size() - 1
			    && cellData.get(getPositionX()).get(getPositionY() + 1).active)
			aDef = "Up";
		    break;
		case 3:
		    if (getPositionY() > 0 && cellData.get(getPositionX()).get(getPositionY() - 1).active)
			aDef = "Down";
		    break;
		}
	    }
	}
	this.niveau = mesPokemons.get(0).getNiveau();
	return new JoueurAction(aType, aDef);
    }

    /**
     * Echange la place de deux pokémons dans la liste mesPokemons. Ancien Prérequis fixed : le pokemon p est AVANT le pokemon p2 dans la liste
     * 
     * @param p
     *            place dans mesPokemons du premier pokemon à échanger(base 0)
     * @param p2
     *            place du second
     */
    public void echangePkmn(int p, int p2) {
	if (!(p == p2))
	{
	    Pokemon tmp = mesPokemons.get(p);
	    Pokemon tmp2 = mesPokemons.get(p2);
	    mesPokemons.remove(p);
	    mesPokemons.add(p, tmp2);
	    mesPokemons.remove(p2);
	    mesPokemons.add(p2, tmp);
	}
    }

    @Override
    public boolean getVivant() {
	return vivant;
    }

    @Override
    public String mort() {
	vivant = false;
	return "GreedyJoueur " + id + " disqualified.";
    }

    @Override
    public void prepareBattle(String type) {
	// Marche même si pas de type adverse
	for (int c = 0; !mesPokemons.isEmpty() && c < mesPokemons.size(); c++)
	{
	    if (type == "Feu" && mesPokemons.get(c).getType() == "Eau")
	    {
		echangePkmn(0, c);
		break; // Peux aussi utiliser un booléen
	    } else if (type == "Plante" && mesPokemons.get(c).getType() == "Feu")
	    {
		echangePkmn(0, c);
		break;
	    } else if (mesPokemons.get(c).getType() == "Plante") // Adv eau
	    {
		echangePkmn(0, c);
		break;
	    }
	}
	utiliseBonbon();
    }

    @Override
    public List<Pokemon> montreTroisPokemon() {
	if (mesPokemons.isEmpty())
	    return null;
	int i = 0;
	List<Pokemon> retour = new ArrayList<>();
	while (i < mesPokemons.size() && i < 3)
	{
	    retour.add(mesPokemons.get(i));
	    i++;
	}
	return retour;
    }

    @Override
    public void creerBonbon() {
	if (!mesBonbons.containsKey("Feu"))
	{
	    mesBonbons.put("Feu", new ArrayList<>());
	    mesBonbons.put("Eau", new ArrayList<>());
	    mesBonbons.put("Herbe", new ArrayList<>());
	}
	if (mesPokemons.size() > 2)
	{
	    trie();
	    echangePkmn(0, mesPokemons.size() - 1); // Met le dernier en premier
	    mesBonbons.get(mesPokemons.get(0).getType()).add(Bonbon.creerBonbon(mesPokemons)); // Collecte du bonbon
	    mesBonbons.put(mesPokemons.get(0).getType(), mesBonbons.get(mesPokemons.get(0).getType())); // Stockage
	    trie();
	}
    }

    @Override
    public void utiliseBonbon() {
	while (!mesBonbons.get(mesPokemons.get(0).getType()).isEmpty())
	{
	    System.out.println(Bonbon.consumeBonbon(mesPokemons, mesBonbons.get(mesPokemons.get(0).getType())));

	}
    }
}
