package game;

import java.io.*;
import javax.sound.sampled.*;
import javax.swing.*;

// To play sound using Clip, the process need to be alive.
// Hence, we use a Swing application.
public class SoundClipTest extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    // Constructor
    public SoundClipTest() {
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	this.setTitle("Pokemon song");
	this.setSize(100, 100);
	this.setVisible(true);

	try
	{
	    // Open an audio input stream.
	    InputStream url = this.getClass().getResourceAsStream("/soundbox/sound.wav"); // sound2 est mieux mais étrangement pas jouable :/
	    AudioInputStream audioIn = AudioSystem.getAudioInputStream(url);
	    // File soundFile = new File("C:/Users/Paul-Elie Pipelin/Desktop/WORKSPACE JAVA/PI2_PROJET/src/soundbox/sound.wav");
	    // AudioInputStream audioIn = AudioSystem.getAudioInputStream(soundFile);
	    // Get a sound clip resource.
	    Clip clip = AudioSystem.getClip();
	    // Open audio clip and load samples from the audio input stream.
	    clip.open(audioIn);
	    clip.loop(Clip.LOOP_CONTINUOUSLY); // Boucle
	} catch (UnsupportedAudioFileException e)
	{
	    e.printStackTrace();
	} catch (IOException e)
	{
	    e.printStackTrace();
	} catch (LineUnavailableException e)
	{
	    e.printStackTrace();
	}
    }

    public static void main(String[] args) {
	new SoundClipTest();
    }
}
